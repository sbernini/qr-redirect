const jQuery = $ = require('jquery');
global.$ = global.jQuery = $;

require('bootstrap/dist/js/bootstrap');

import './styles/app.scss';

// start the Stimulus application
import './bootstrap';
