<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Entity\Product;
use App\Generator\DefaultQRManager;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityUpdatedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityDeletedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Filesystem\Filesystem;

class ProductQRGenerateSubscriber implements EventSubscriberInterface
{
    private DefaultQRManager $defaultQRManager;

    public function __construct(DefaultQRManager $defaultQRManager)
    {
        $this->defaultQRManager = $defaultQRManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => ['prePersist'],
            BeforeEntityUpdatedEvent::class => ['preUpdate'],
            BeforeEntityDeletedEvent::class => ['preRemove']
        ];
    }

    public function prePersist(BeforeEntityPersistedEvent $event): void
    {
        $entity = $event->getEntityInstance();

        if (!($entity instanceof Product)) {
            return;
        }

        $this->createQRIfNotExistent($entity);
    }

    public function preUpdate(BeforeEntityUpdatedEvent $event): void
    {
        $entity = $event->getEntityInstance();

        if (!($entity instanceof Product)) {
            return;
        }

        $this->createQRIfNotExistent($entity);
    }

    public function preRemove(BeforeEntityDeletedEvent $event): void
    {
        $entity = $event->getEntityInstance();

        if (!($entity instanceof Product)) {
            return;
        }

        if($entity->getQrCode() === null)
        {
            //QR not present
            return;
        }

        $fs = new Filesystem();
        $this->defaultQRManager->cleanupProductQR($fs, $entity);
    }

    private function createQRIfNotExistent(Product $product): void
    {
        if($product->getQrCode() !== null)
        {
            return;
        }

        $this->defaultQRManager->generateProductQR($product);
    }
}
