<?php

namespace App\Controller\Admin;

use App\Entity\Product;
use App\Form\ProductAttachmentType;
use App\Generator\DefaultQRManager;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Field\FieldInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Snappy\Pdf;
use Twig\Environment;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ProductCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Product::class;
    }

    public function exportQR(AdminContext $context, Pdf $pdfGenerator, Environment $twig, DefaultQRManager $QRGenerator): PdfResponse
    {
        /** @var Product $product */
        $product = $context->getEntity()->getInstance();
        $html = $twig->render('pdf/product.html.twig', ['product' => $product, 'qr' => base64_encode($QRGenerator->getProductQRAsString($product))]);

        return new PdfResponse(
            $pdfGenerator->getOutputFromHtml($html),
            (string)$product.'_qr_code.pdf'
        );
    }

    /*public function configureAssets(Assets $assets): Assets
    {
        return $assets
            ->addCssFile('build/admin.css')
        ;
    }*/

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Prodotto')
            ->setEntityLabelInPlural('Prodotti')
            ->setSearchFields(['id', 'name', 'code', 'barcode'])
            ->setFormThemes([
                '@FOSCKEditor/Form/ckeditor_widget.html.twig',
                '@EasyAdmin/crud/form_theme.html.twig'])
            ->showEntityActionsInlined()
        ;
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('name')
            ->add('code')
            ->add('barcode')
            ->add('enabled')
        ;
    }

    /** @return iterable|array<int,FieldInterface> */
    public function configureFields(string $pageName): iterable
    {
        $id = IdField::new('id', 'ID')->setMaxLength(255);
        $name = TextField::new('name', 'Nome');
        $code = TextField::new('code', 'SKU');
        $barcode = TextField::new('barcode', 'Codice a barre');
        $enabled = BooleanField::new('enabled', 'Attivo');
        $description = TextareaField::new('description', 'Descrizione')->setFormType(CKEditorType::class);
        $imageFile = Field::new('imageFile', 'Immagine')->setFormType(VichImageType::class);
        $image = ImageField::new('image')->setBasePath('/uploads/images/product');
        $qrCode = ImageField::new('qrCode')->setBasePath('/uploads/images/qr');
        $attachments = CollectionField::new('attachments')
            ->setLabel('Allegati')
            ->setEntryType(ProductAttachmentType::class);

        $createdAt = DateTimeField::new('createdAt', 'Creazione')->setFormat('d-M-Y H:m');
        $updatedAt = DateTimeField::new('updatedAt', 'Ultima modifica')->setFormat('d-M-Y H:m');

        if ($pageName === Crud::PAGE_INDEX) {
            return [$id, $name, $code, $barcode, $enabled, $image, $qrCode];
        }

        if ($pageName === Crud::PAGE_DETAIL) {
            return [$id, $name, $code, $barcode, $enabled, $description, $createdAt, $updatedAt];
        }

        if ($pageName === Crud::PAGE_NEW) {
            return [$name, $code, $barcode, $enabled, $imageFile, $description];
        }

        if ($pageName === Crud::PAGE_EDIT) {
            return [$name, $code, $barcode, $enabled, $imageFile, $attachments, $description];
        }
    }

    public function configureActions(Actions $actions): Actions
    {
        $exportQR = Action::new('exportQR', 'Esporta QR')
            ->linkToCrudAction('exportQR')
            ->setIcon('fas fa-qrcode');

        $exportAllQR = Action::new('exportAllQR', 'Esporta tutti i QR')
            ->linkToCrudAction('exportAllQR')
            ->addCssClass('btn btn-primary')
            ->setIcon('fas fa-qrcode');

        return $actions
            ->add(Crud::PAGE_INDEX, $exportQR)
            ->addBatchAction($exportAllQR)
        ;
    }

}
