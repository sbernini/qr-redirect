<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Field\FieldInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserCrudController extends AbstractCrudController
{
    private UserPasswordHasherInterface $userPasswordHasher;

    public function __construct(UserPasswordHasherInterface $userPasswordHasher)
    {
        $this->userPasswordHasher = $userPasswordHasher;
    }

    private const ROLES = [
        'Utente' => 'ROLE_USER',
        'Admin' => 'ROLE_ADMIN',
        'SuperAdmin' => 'ROLE_SUPER_ADMIN',
    ];

    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        if ($entityInstance->getPlainPassword() !== null) {
            $entityInstance->setPassword($this->userPasswordHasher->hashPassword($entityInstance, $entityInstance->getPlainPassword()));
            $entityInstance->setPlainPassword(null);
        }

        parent::persistEntity($entityManager, $entityInstance);
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        if ($entityInstance->getPlainPassword() !== null) {
            $entityInstance->setPassword($this->userPasswordHasher->hashPassword($entityInstance, $entityInstance->getPlainPassword()));
            $entityInstance->setPlainPassword(null);
        }

        parent::updateEntity($entityManager, $entityInstance);
    }

    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Utente')
            ->setEntityLabelInPlural('Utenti')
            ->setSearchFields(['id', 'username', 'roles', 'firstName', 'lastName'])
        ;
    }

    /** @return iterable|array<int,FieldInterface> */
    public function configureFields(string $pageName): iterable
    {
        $username = TextField::new('username', 'Username');
        $plainPassword = Field::new('plainPassword', 'Password');
        $roles = ChoiceField::new('roles', 'Ruoli')->setRequired(false)
            ->allowMultipleChoices(true)
            ->setChoices(self::ROLES);
        $id = IntegerField::new('id', 'ID');
        $password = TextField::new('password', 'Password');

        if ($pageName === Crud::PAGE_INDEX) {
            return [$username, $roles];
        }

        if ($pageName === Crud::PAGE_DETAIL) {
            return [$id, $username, $roles, $password];
        }

        if ($pageName === Crud::PAGE_NEW) {
            return [$username, $plainPassword, $roles];
        }

        if ($pageName === Crud::PAGE_EDIT) {
            return [$username, $plainPassword, $roles];
        }
    }
}
