<?php

namespace App\Controller;

use App\Entity\Product;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

#[Route('/product')]
class ProductController extends AbstractController
{
    #[IsGranted('ROLE_USER')]
    #[Route('', name: 'app_product_index', methods: ['GET'])]
    public function index(): Response
    {
        return $this->render('product/index.html.twig');
    }

    #[Route('/{id}', name: 'app_product_show', methods: ['GET'])]
    public function show(Product $product, RouterInterface $router): Response
    {
        return $this->render('product/show.html.twig', [
            'product' => $product,
            'qr_url' => $router->generate('app_product_show',['id' => $product->getId()])
        ]);
    }
}
