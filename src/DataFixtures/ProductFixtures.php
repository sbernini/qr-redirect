<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ProductFixtures extends Fixture
{
    private const PRODUCT_NUMBER = 200;

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('it_IT');
        for($i=0; $i<= self::PRODUCT_NUMBER; $i++)
        {
            $product = new Product();
            $product->setName($faker->words(2, true));
            $product->setCode($faker->ean8);
            $product->setBarcode($faker->ean13);
            $product->setDescription($faker->realText());

            $manager->persist($product);
        }

        $manager->flush();
    }
}
