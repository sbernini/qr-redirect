<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture implements FixtureGroupInterface
{
    private UserPasswordHasherInterface $encoder;

    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    private const ADMIN_PWD = 'q1w2e3r4';
    private const QUANTIK_PWD = 'Rogth9quagud';

    public function load(ObjectManager $manager): void
    {
        $userAdmin = new User();
        $userAdmin->setUsername('simone');
        $userAdmin->setRoles(['ROLE_SUPER_ADMIN']);
        $userAdmin->setPassword($this->encoder->hashPassword($userAdmin, self::ADMIN_PWD));

        $this->setReference('user_simone', $userAdmin);
        $manager->persist($userAdmin);

        $userQuantik = new User();
        $userQuantik->setUsername('quantik');
        $userQuantik->setRoles(['ROLE_SUPER_ADMIN']);
        $userQuantik->setPassword($this->encoder->hashPassword($userQuantik, self::QUANTIK_PWD));

        $this->setReference('user_quantik', $userQuantik);
        $manager->persist($userQuantik);

        $manager->flush();
    }

    /** @return string[] */
    public static function getGroups(): array
    {
        return ['dev', 'prod'];
    }
}
