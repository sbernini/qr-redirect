<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use WebDevelovers\Resource\Model\TimestampableInterface;
use WebDevelovers\Resource\Model\TimestampableTrait;
use WebDevelovers\Resource\Model\ToggleableInterface;
use WebDevelovers\Resource\Model\ToggleableTrait;
use WebDevelovers\Resource\Model\UUIDIdentifiableInterface;
use WebDevelovers\Resource\Model\UUIDIdentifiableTrait;

#[ORM\Entity(repositoryClass: UserRepository::class), ORM\HasLifecycleCallbacks]
class User implements
    TimestampableInterface,
    ToggleableInterface,
    PasswordAuthenticatedUserInterface,
    UserInterface,
    UUIDIdentifiableInterface
{
    use TimestampableTrait;
    use ToggleableTrait;
    use UUIDIdentifiableTrait {
        UUIDIdentifiableTrait::__construct as private initializeID;
    }

    public function __construct()
    {
        $this->initializeID();
        $this->enable();
    }

    #[ORM\Column(type: 'string', length: 180, unique: true)]
    private $username;

    #[ORM\Column(type: 'json')]
    private $roles = [];

    #[ORM\Column(type: 'string')]
    private $password;

    private ?string $plainPassword = null;

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getUserIdentifier(): string
    {
        return (string) $this->username;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): void
    {
        $this->roles = $roles;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function eraseCredentials()
    {
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(?string $plainPassword): void
    {
        $this->plainPassword = $plainPassword;
    }
}
