<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use WebDevelovers\Resource\Model\TimestampableTrait;
use WebDevelovers\Resource\Model\UUIDIdentifiableTrait;

#[ORM\Entity, ORM\HasLifecycleCallbacks]
#[Vich\Uploadable]
class ProductAttachment
{
    use TimestampableTrait;
    use UUIDIdentifiableTrait {
        UUIDIdentifiableTrait::__construct as private initializeID;
    }

    public function __toString(): string
    {
        return $this->attachmentName ?? (string)$this->id;
    }

    public function __construct()
    {
        $this->initializeID();
    }

    #[Assert\NotNull]
    #[ORM\ManyToOne(targetEntity: Product::class, inversedBy: 'attachments'), ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    private ?Product $product = null;

    #[Vich\UploadableField(mapping: 'product_qr', fileNameProperty: 'attachment ', originalName: 'attachmentName')]
    private ?File $file = null;

    #[Assert\Type(type: 'string')]
    #[ORM\Column(type: Types::STRING, nullable: true)]
    private ?string $attachment = null;

    #[Assert\Type(type: 'string')]
    #[ORM\Column(type: Types::STRING, nullable: true)]
    private ?string $attachmentName = null;

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): void
    {
        $this->product = $product;
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function setFile(?File $file): void
    {
        $this->file = $file;
    }

    public function getAttachment(): ?string
    {
        return $this->attachment;
    }

    public function setAttachment(?string $attachment): void
    {
        $this->attachment = $attachment;
    }

    public function getAttachmentName(): ?string
    {
        return $this->attachmentName;
    }

    public function setAttachmentName(?string $attachmentName): void
    {
        $this->attachmentName = $attachmentName;
    }
}
