<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use WebDevelovers\Resource\Model\CodeTrait;
use WebDevelovers\Resource\Model\DescriptionTrait;
use WebDevelovers\Resource\Model\StandardNamingTrait;
use WebDevelovers\Resource\Model\TimestampableTrait;
use WebDevelovers\Resource\Model\ToggleableTrait;
use WebDevelovers\Resource\Model\UUIDIdentifiableTrait;

#[ORM\Entity(repositoryClass: ProductRepository::class), ORM\HasLifecycleCallbacks]
#[Vich\Uploadable]
class Product
{
    use CodeTrait;
    use DescriptionTrait;
    use ProductImageTrait;
    use StandardNamingTrait;
    use TimestampableTrait;
    use ToggleableTrait;
    use UUIDIdentifiableTrait {
        UUIDIdentifiableTrait::__construct as private initializeID;
    }

    public function __toString(): string
    {
        if(null === $this->name && null === $this->code)
        {
            return 'ND';
        }

        if(null === $this->code)
        {
            return $this->name;
        }

        return '['.$this->code.'] '. $this->name;
    }

    public function __construct()
    {
        $this->initializeID();
        $this->enable();

        $this->attachments = new ArrayCollection();
    }

    #[Assert\Type(type: 'string')]
    #[ORM\Column(type: Types::STRING, nullable: true)]
    protected ?string $barcode = null;

    #[Vich\UploadableField(mapping: 'product_qr', fileNameProperty: 'qrCode')]
    private ?File $qrFile = null;

    #[Assert\Type(type: 'string')]
    #[ORM\Column(type: Types::STRING, nullable: true)]
    protected ?string $qrCode = null;

    /** @var Collection|ProductAttachment[] */
    #[Assert\Valid]
    #[ORM\OneToMany(targetEntity: ProductAttachment::class, mappedBy: 'product', orphanRemoval: true, cascade: ['persist', 'remove'])]
    private Collection $attachments;

    public function getBarcode(): ?string
    {
        return $this->barcode;
    }

    public function setBarcode(?string $barcode): void
    {
        $this->barcode = $barcode;
    }

    public function getQrFile(): ?File
    {
        return $this->qrFile;
    }

    public function setQrFile(?File $qrFile): void
    {
        $this->qrFile = $qrFile;
    }

    public function getQrCode(): ?string
    {
        return $this->qrCode;
    }

    public function setQrCode(?string $qrCode): void
    {
        $this->qrCode = $qrCode;
    }

    /**
     * @return Collection|ProductAttachment[]
     */
    public function getAttachments(): Collection
    {
        return $this->attachments;
    }

    public function addAttachment(ProductAttachment $attachment): void
    {
        if ($this->attachments->contains($attachment)) {
            return;
        }

        $this->attachments[] = $attachment;
        $attachment->setProduct($this);
    }

    public function removeAttachment(ProductAttachment $attachment): void
    {
        if (! $this->attachments->removeElement($attachment)) {
            return;
        }

        if ($attachment->getProduct() !== $this) {
            return;
        }

        $attachment->setProduct(null);
    }
}
