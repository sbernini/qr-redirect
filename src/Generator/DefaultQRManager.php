<?php

declare(strict_types=1);

namespace App\Generator;

use App\Entity\Product;
use Endroid\QrCode\Builder\Builder;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelMedium;
use Endroid\QrCode\Label\Alignment\LabelAlignmentCenter;
use Endroid\QrCode\Label\Font\NotoSans;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\SvgWriter;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;


class DefaultQRManager
{
    private RouterInterface $router;
    private string $qrPath;

    private const DEFAULT_FORMAT = 'svg';
    private string $baseUrl;

    public function __construct(RouterInterface $router, string $qrPath, string $baseUrl)
    {
        $this->router = $router;
        $this->qrPath = $qrPath;
        $this->baseUrl = $baseUrl;
    }

    public function generateProductQR(Product $product): void
    {
        $qrImage = $this->generateQR($product);
        $this->uploadQR($product, $qrImage);
        $product->setQrCode($this->getFilename($product));
    }

    public function getProductQRAsString(Product $product): string
    {
        return file_get_contents($this->getQRPath($product));
    }

    public function cleanupProductQR(Filesystem $filesystem, Product $product): void
    {
        $pathname = $this->getQRPath($product);
        if($filesystem->exists($pathname)){
            $filesystem->remove($pathname);
        }
    }

    public function generateQR(Product $product): string
    {
        $result = Builder::create()
            ->writer(new SvgWriter())
            ->writerOptions([])
            ->data($this->baseUrl.$this->router->generate('app_product_show', ['id' => $product->getId()]))
            ->encoding(new Encoding('UTF-8'))
            ->errorCorrectionLevel(new ErrorCorrectionLevelMedium())
            ->size(300)
            ->margin(10)
            ->roundBlockSizeMode(new RoundBlockSizeModeMargin())
            ->labelText($product->getName())
            ->labelFont(new NotoSans(20))
            ->labelAlignment(new LabelAlignmentCenter())
            ->build();

        return $result->getString();
    }

    private function getFilename(Product $product): string
    {
        return (string)$product->getId().'.'.self::DEFAULT_FORMAT;
    }

    private function getQRPath(Product $product): string
    {
        return $this->qrPath.'/'.$this->getFilename($product);
    }

    private function uploadQR(Product $product, string $qrImage): void
    {
        file_put_contents($this->getQRPath($product), $qrImage);
    }
}
