<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\Product;
use App\Generator\DefaultQRManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class GenerateQRCodesCommand extends Command
{
    /** @var string $defaultName */
    protected static $defaultName = 'quantik:generate:qr-codes';
    private DefaultQRManager $QRGenerator;
    private Filesystem $filesystem;

    public function __construct(EntityManagerInterface $manager, DefaultQRManager $QRGenerator)
    {
        parent::__construct(self::$defaultName);
        $this->manager = $manager;
        $this->QRGenerator = $QRGenerator;
        $this->filesystem = new Filesystem();
    }

    protected function configure(): void
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Genera immagini QR code per ogni prodotto in DB che non ne ha una (force per sovrascrivere l\'esistente')
            ->addOption(
                'regenerate',
                'regenerate',
                InputOption::VALUE_OPTIONAL,
                'Se bisogna sostituire i QR esistenti (ATTENZIONE! i qr esistenti verranno cancellati e sovrascritti)',
                '0'
            )
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $products = $this->manager->getRepository(Product::class)->findAll();
        $regenerate = $input->getOption('regenerate');

        /** @var Product $product */
        foreach($products as $product)
        {
            if(null === $product->getQrCode())
            {
                $this->QRGenerator->generateProductQR($product);
            } elseif ($product->getQrCode() !== null && $regenerate === '1')
            {
                $this->QRGenerator->cleanupProductQR($this->filesystem, $product);
                $this->QRGenerator->generateProductQR($product);
            }

        }

        $this->manager->flush();

        return Command::SUCCESS;
    }
}
